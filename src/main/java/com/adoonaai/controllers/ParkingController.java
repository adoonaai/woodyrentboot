package com.adoonaai.controllers;


import com.adoonaai.models.Parking;
import com.adoonaai.sevice.parking.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/parkings")
public class ParkingController {

    private final ParkingService parkingService;

    @Autowired
    public ParkingController(ParkingService parkingService) {
        this.parkingService = parkingService;
    }


    @PostMapping
    public Parking createParking(@RequestBody Parking parking) {

//        checkUserRole
       return parkingService.createParking(parking);

    }

    @GetMapping
    public List<Parking> getAllParking() {

        return parkingService.getAllParkingZone();
    }


    @GetMapping("/{id}")
    public Parking findByIdParking(@PathVariable(name = "id") Long id) {

        return parkingService.findById(id);
    }

    @PutMapping("/{id}")
    public void updateParking(@RequestBody Parking parking) {

        parkingService.updateParking(parking);

    }

    @DeleteMapping("/{id}")
    public void deleteParking(@PathVariable(name = "id") Long id) {

        parkingService.deleteParking(id);
    }


}
