package com.adoonaai.controllers;



import com.adoonaai.dto.UserUi;
import com.adoonaai.models.User;
import com.adoonaai.security.CallContext;
import com.adoonaai.security.SecurityContext;
import com.adoonaai.sevice.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;


@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping()
    public void createUser(@RequestBody User user){

        userService.createUser(user);
    }

    @GetMapping()
    public List<User> getAllUsers(){

        return userService.getAllUsers();
    }

    @GetMapping("/current")
    public UserUi getCurrent(){


      return userService.getCurrent();

    }


    @GetMapping("/{id}")
    public User findByIdUser(@PathVariable(name = "id") Long id){

        return userService.findById(id);
    }

    @PutMapping("/{id}")
    public void updateUser(@RequestBody User user){

        userService.updateUser(user);

    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable(name = "id") Long id){

       userService.deleteUser(id);


    }






}