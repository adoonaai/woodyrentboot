package com.adoonaai.controllers;


import com.adoonaai.dto.CreateTripRequestDto;
import com.adoonaai.models.Trip;
import com.adoonaai.models.enums.Role;
import com.adoonaai.security.CallContext;
import com.adoonaai.security.SecurityContext;
import com.adoonaai.sevice.trip.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/trips")
public class TripController {

    private final TripService tripService;

    @Autowired
    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @PostMapping
    public void createTrip(@RequestBody CreateTripRequestDto requestDto){

        tripService.createTrip(requestDto);
    }

    @PutMapping("/closing/{id}")
    public void closingTrip(@PathVariable Long id){

        tripService.closingTrip(id);
    }

//    @PostMapping("/view")
//    public List<Trip> allTripForUser(){
//        CallContext context = SecurityContext.get();
//        return tripService.findTripByUserId(Long.valueOf(context.getUserId()));
//    }

    @PostMapping("/view")
    public List<Trip> allTripForAdmin(){
        CallContext context = SecurityContext.get();
        return tripService.findAllTrips(context.getUserId(), Role.valueOf(context.getRole()));
    }


}
