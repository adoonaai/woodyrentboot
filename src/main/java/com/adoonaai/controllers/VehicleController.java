package com.adoonaai.controllers;


import com.adoonaai.dto.VehicleDto;
import com.adoonaai.models.Vehicle;
import com.adoonaai.sevice.vehicle.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;

    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @PostMapping
    public void creteVehicle(@RequestBody VehicleDto vehicleDto){

        vehicleService.createVehicle(vehicleDto);

    }

    @GetMapping
    public List<Vehicle> getAllVehicle(){

        return vehicleService.getAllVehicle();

    }


    @GetMapping("/{id}")
    public Vehicle findByIdVehicle(@PathVariable(name = "id") Long id){

        return vehicleService.findById(id);
    }



//    @GetMapping("/scooter")
//    public ResponseEntity<List<Vehicle>> getAllScooters(){
//
//        final List<Vehicle> scooters = vehicleService.getAllScooters();
//        if (scooters != null && !scooters.isEmpty()){
//            return new ResponseEntity<>(scooters, HttpStatus.OK);
//        }
//        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }
//
//    @GetMapping("/bike")
//    public ResponseEntity<List<Vehicle>> getAllBikes(){
//
//        final List<Vehicle> bikes = vehicleService.getAllBike();
//        if (bikes != null && !bikes.isEmpty()){
//            return new ResponseEntity<>(bikes, HttpStatus.OK);
//        }
//        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }



    @DeleteMapping("/{id}")
    public void deleteVehicle(@PathVariable(name = "id") Long id) {

        vehicleService.deleteVehicle(id);
    }

}
