package com.adoonaai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WoodyRentBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(WoodyRentBootApplication.class, args);
    }

}
