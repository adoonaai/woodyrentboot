package com.adoonaai.access;

import com.adoonaai.models.Vehicle;
import com.adoonaai.models.enums.VehicleType;
import com.adoonaai.models.enums.VehicleStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    @Query("select v from Vehicle v where v.type = :type")
    List<Vehicle> findByTypeVehicle(@Param("type") VehicleType vehicleType);

    @Query("select v from Vehicle v where v.status = :vehicleStatus")
    List<Vehicle> findByVehicleStatus(@Param("vehicleStatus") VehicleStatus vehicleStatus);

    @Query("select v from Vehicle v where v.model = :model")
    Vehicle findByModel(@Param("model") String model);

}
