package com.adoonaai.access;

import com.adoonaai.models.Trip;
import com.adoonaai.models.Vehicle;
import com.adoonaai.models.enums.Condition;
import com.adoonaai.models.enums.TripStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TripRepository extends JpaRepository<Trip, Long> {

    @Query("select v from Vehicle v where v.condition = :condition")
    List<Vehicle> findByCondition(@Param("condition") Condition condition);

    List<Trip> findByUserId(Long id);

    @Query("select t from Trip t where t.status = 'CREATED' and t.vehicle = :vehicle")
    Trip findByBusyVehicleId(@Param("vehicle") Vehicle vehicle);

}
