package com.adoonaai.access;

import com.adoonaai.models.Parking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParkingRepository extends JpaRepository<Parking, Long> {


}
