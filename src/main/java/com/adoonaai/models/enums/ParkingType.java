package com.adoonaai.models.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ParkingType {

    @JsonProperty("scooter")
    SCOOTER,
    @JsonProperty("bike")
    BIKE,
    @JsonProperty("all")
    ALL;

}
