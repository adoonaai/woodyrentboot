package com.adoonaai.models.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum VehicleType {

    @JsonProperty("scooter")
    SCOOTER,
    @JsonProperty("bike")
    BIKE
}
