package com.adoonaai.models.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TripStatus {

    @JsonProperty("created")
    CREATED,
    @JsonProperty("completed")
    COMPLETED
}
