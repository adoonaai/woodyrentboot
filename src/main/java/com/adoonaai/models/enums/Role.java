package com.adoonaai.models.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Role {

    @JsonProperty("admin")
    ADMIN,
    @JsonProperty("user")
    USER;
}
