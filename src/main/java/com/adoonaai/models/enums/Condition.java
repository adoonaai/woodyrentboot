package com.adoonaai.models.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Condition {

    @JsonProperty("excellent")
    EXCELLENT,
    @JsonProperty("good")
    GOOD,
    @JsonProperty("satisfactory")
    SATISFACTORY
}
