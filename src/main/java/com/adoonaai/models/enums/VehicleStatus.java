package com.adoonaai.models.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum VehicleStatus {

    @JsonProperty("free")
    FREE,
    @JsonProperty("busy")
    BUSY
}
