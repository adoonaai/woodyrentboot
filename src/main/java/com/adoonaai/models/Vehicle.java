package com.adoonaai.models;

import com.adoonaai.models.enums.Condition;
import com.adoonaai.models.enums.VehicleType;
import com.adoonaai.models.enums.VehicleStatus;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.math.BigDecimal;


@Entity
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
//@DiscriminatorValue("BIKIIII")
//@JsonDeserialize(as = Bike.class)
@Table(name = "vehicle")
public abstract class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String model;
    private double longitude;
    private double latitude;
    @Column(name = "start_price")
    private BigDecimal startPrice;
    @Column(name = "price_per_minute")
    private BigDecimal pricePerMinute;
    @Enumerated(EnumType.STRING)
    @Column(name = "type", insertable = false, updatable = false)
    private VehicleType type;
    @Enumerated(EnumType.STRING)
    @Column(name = "condition" )
    private Condition condition;
    @Enumerated(EnumType.STRING)
    @Column(name = "status",nullable = false)
    @ColumnDefault("'FREE'")
    private VehicleStatus status = VehicleStatus.FREE;

    @ManyToOne
    @JoinColumn(name = "parking_id")
    private Parking parking;



    public Parking getParking() {
        return parking;
    }

    public void setParking(Parking parking) {
        this.parking = parking;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    public BigDecimal getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(BigDecimal startPrice) {
        this.startPrice = startPrice;
    }

    public BigDecimal getPricePerMinute() {
        return pricePerMinute;
    }

    public void setPricePerMinute(BigDecimal pricePerMinute) {
        this.pricePerMinute = pricePerMinute;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public VehicleStatus getStatus() {
        return status;
    }

    public void setStatus(VehicleStatus status) {
        this.status = status;
    }
}

