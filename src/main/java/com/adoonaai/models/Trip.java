package com.adoonaai.models;

import com.adoonaai.models.enums.TripStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Builder
@Entity
@Table(name = "trip")
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "start_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime startTime;
    @Column(name = "end_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endTime;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private TripStatus status;
    @Column(name = "cost_travel")
    private BigDecimal constTravel;

//    @PrimaryKeyJoinColumn
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private User user;

    //    @PrimaryKeyJoinColumn
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "vehicle_id",referencedColumnName = "id")
//    @OneToMany(targetEntity = Vehicle.class)
    private Vehicle vehicle;

    @OneToOne
    private Parking startParking;

    @OneToOne
    private Parking endParking;

    public Trip() {

    }

    public Trip(Long id, LocalDateTime startTime, LocalDateTime endTime, TripStatus status, BigDecimal constTravel, User user, Vehicle vehicle, Parking startParking, Parking endParking) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.status = status;
        this.constTravel = constTravel;
        this.user = user;
        this.vehicle = vehicle;
        this.startParking = startParking;
        this.endParking = endParking;
    }

    public Parking getStartParking() {
        return startParking;
    }

    public void setStartParking(Parking startParking) {
        this.startParking = startParking;
    }

    public Parking getEndParking() {
        return endParking;
    }

    public void setEndParking(Parking endParking) {
        this.endParking = endParking;
    }

    public BigDecimal getConstTravel() {
        return constTravel;
    }

    public void setConstTravel(BigDecimal constTravel) {
        this.constTravel = constTravel;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public TripStatus getStatus() {
        return status;
    }

    public void setStatus(TripStatus status) {
        this.status = status;
    }
}
