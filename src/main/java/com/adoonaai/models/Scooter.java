package com.adoonaai.models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("SCOOTER")
public class Scooter extends Vehicle {

    private int maxSpeed;
    private int engineCharge;

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getEngineCharge() {
        return engineCharge;
    }

    public void setEngineCharge(int engineCharge) {
        this.engineCharge = engineCharge;
    }
}



