package com.adoonaai.telegram;

import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;




public abstract class AbstractBotCommand extends BotCommand {


    protected final SendMsg utils;

    public AbstractBotCommand(String commandIdentifier, String description, SendMsg utils) {
        super(commandIdentifier, description);
        this.utils = utils;
    }
}
