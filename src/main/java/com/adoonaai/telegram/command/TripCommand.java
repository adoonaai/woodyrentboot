package com.adoonaai.telegram.command;

import com.adoonaai.dto.CreateTripRequestDto;
import com.adoonaai.models.Vehicle;
import com.adoonaai.sevice.trip.TripService;
import com.adoonaai.sevice.user.UserService;
import com.adoonaai.sevice.vehicle.VehicleService;
import com.adoonaai.telegram.AbstractBotCommand;
import com.adoonaai.telegram.SendMsg;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.time.LocalDateTime;


@Component
public class TripCommand extends AbstractBotCommand {


    private final UserService userService;
    private final VehicleService vehicleService;
    private final TripService tripService;


    public TripCommand(SendMsg utils, UserService userService, VehicleService vehicleService, TripService tripService) {
        super("/trip", "createTrip", utils);
        this.userService = userService;
        this.vehicleService = vehicleService;
        this.tripService = tripService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

        try {

            if (userService.userExistByLogin(user.getUserName())) {
                Vehicle vehicle = vehicleService.findByModel(strings[0]);
                com.adoonaai.models.User myUser = userService.findByLogin(user.getUserName());


                CreateTripRequestDto createTripRequestDto = new CreateTripRequestDto();
                createTripRequestDto.setUserId(myUser.getId());
                createTripRequestDto.setVehicleId(vehicle.getId());
                tripService.createTrip(createTripRequestDto);

                String info = String.format(
                        "Аренда прошла успешно! \n" +
                                "Модель ТС: %s \n" +
                                "Время старта: %s \n", vehicle.getModel(), LocalDateTime.now());

                utils.send(absSender, chat.getId(), info, ParseMode.HTML, false);

            } else {
                utils.send(absSender, chat.getId(), "Вы не зарегистрированы в системе! Пройдите регистрацию", ParseMode.HTML, false);
            }
        } catch (Exception e) {
            utils.send(absSender, chat.getId(), "Непредвиденная ошибка, попробуйте снова!", ParseMode.HTML, false);
            System.out.println("Ошибка в TripCommand");
        }


    }


}
