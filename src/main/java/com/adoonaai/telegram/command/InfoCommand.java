package com.adoonaai.telegram.command;

import com.adoonaai.sevice.trip.TripService;
import com.adoonaai.sevice.user.UserService;
import com.adoonaai.telegram.AbstractBotCommand;
import com.adoonaai.telegram.SendMsg;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;


@Component
public class InfoCommand extends AbstractBotCommand {


    private final UserService userService;
    private final TripService tripService;

    public InfoCommand(SendMsg utils, UserService userService, TripService tripService) {
        super("/info","info command" , utils);
        this.userService = userService;
        this.tripService = tripService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

        try {
            utils.send(absSender,chat.getId(),"Информация о пользователе", ParseMode.HTML,false);
            if (userService.userExistByLogin(user.getUserName())){

                com.adoonaai.models.User myUser = userService.findByLogin(user.getUserName());
                String strInfo = String.format("Ваш логин: %s \n" +
                                           "Ваш пароль: %s \n" +
                                           "Ваш баланс %s \n" +
                                           "Вы имеете роль: %s \n", myUser.getLogin(),myUser.getPassword(),myUser.getBalance(),myUser.getRole());
                utils.send(absSender, chat.getId(), strInfo,ParseMode.HTML,false);
            }else {
                utils.send(absSender,chat.getId(),"Вы не зарегистрированы в системе! Пройдите регистрацию",ParseMode.HTML,false);
            }
        }catch (Exception e){
            utils.send(absSender, chat.getId(), "Непредвиденная ошибка, попробуйте снова!", ParseMode.HTML, false);
            System.out.println("Ошибка в InfoCommand");
        }


    }
}
