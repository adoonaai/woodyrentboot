package com.adoonaai.telegram.command;


import com.adoonaai.models.Trip;
import com.adoonaai.models.Vehicle;
import com.adoonaai.sevice.trip.TripService;
import com.adoonaai.sevice.vehicle.VehicleService;
import com.adoonaai.telegram.AbstractBotCommand;
import com.adoonaai.telegram.SendMsg;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.time.LocalDateTime;

@Component
public class FinishCommand extends AbstractBotCommand {

    private final TripService tripService;
    private final VehicleService vehicleService;


    public FinishCommand(SendMsg utils, TripService tripService, VehicleService vehicleService) {
        super("/finish","finish command", utils);
        this.tripService = tripService;
        this.vehicleService = vehicleService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

        try{
            Vehicle vehicle = vehicleService.findByModel(strings[0]);
            Trip trip = tripService.findTripBusyByVehicleId(vehicle);
            tripService.closingTrip(trip.getId());

            String message = String.format(
                    "Вы успешно завершили аренду! \n" +
                    "Модель ТС: %s \n" +
                    "Время старта: %s \n" +
                    "Время окончания: %s \n" +
                    "Стоимость поездки: %s \n", vehicle.getModel(), trip.getStartTime(),LocalDateTime.now(),trip.getConstTravel());

            utils.send(absSender, chat.getId(), message, ParseMode.HTML, false);

        }catch (Exception e){
            utils.send(absSender, chat.getId(), "Непредвиденная ошибка, попробуйте снова!", ParseMode.HTML, false);
            System.out.println("Ошибка в FinishCommand");
        }

    }
}
