package com.adoonaai.telegram.command;

import com.adoonaai.models.Vehicle;
import com.adoonaai.sevice.vehicle.VehicleService;
import com.adoonaai.telegram.AbstractBotCommand;
import com.adoonaai.telegram.SendMsg;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.util.List;


@Component
public class SearchCommand extends AbstractBotCommand {

    private final VehicleService vehicleService;

    public SearchCommand(SendMsg utils, VehicleService vehicleService) {
        super("/search","search vehicle", utils);
        this.vehicleService = vehicleService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

        try {
            utils.send(absSender,chat.getId(),"Поиск доступных ТС", ParseMode.HTML,false);
            List<Vehicle> vehicles = vehicleService.getAllVehicleAvailable();
            if (!vehicles.isEmpty()) {
                for (Vehicle vehicle : vehicles) {
                    String search = String.format(
                                    "Id: %s \n" +
                                    "Модель: %s \n" +
                                    "Тип ТС: %s \n" +
                                    "Состояние: %s \n" +
                                    "Цена старта: %s рублей\n" +
                                    "Стоимость минуты: %s рублей \n" +
                                    "Парковка: %s \n", vehicle.getId(), vehicle.getModel(), vehicle.getType().toString(),
                            vehicle.getCondition(), vehicle.getStartPrice(), vehicle.getPricePerMinute(), vehicle.getParking().getName());
                    utils.send(absSender, chat.getId(), search, ParseMode.HTML, false);
                }
            }else {
                utils.send(absSender, chat.getId(), "Доступных ТС нет! Ожидайте", ParseMode.HTML, false);
            }


        }catch (Exception e){
            utils.send(absSender, chat.getId(), "Непредвиденная ошибка, попробуйте снова!", ParseMode.HTML, false);
            System.out.println("Ошибка в SearchCommand");
        }


    }
}
