package com.adoonaai.telegram.command;

import com.adoonaai.models.Trip;
import com.adoonaai.sevice.trip.TripService;
import com.adoonaai.sevice.user.UserService;
import com.adoonaai.telegram.AbstractBotCommand;
import com.adoonaai.telegram.SendMsg;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.util.List;

@Component
public class TripInfoCommand extends AbstractBotCommand {

    private final UserService userService;
    private final TripService tripService;

    public TripInfoCommand(SendMsg utils, TripService tripService, UserService userService) {
        super("/tripinfo","info about trip", utils);
        this.tripService = tripService;
        this.userService = userService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

    try {
        if (userService.userExistByLogin(user.getUserName())) {
            utils.send(absSender,chat.getId(),"Информация о ваших арендах", ParseMode.HTML,false);
            com.adoonaai.models.User myUser = userService.findByLogin(user.getUserName());

            List<Trip> trips = tripService.findTripByUserId(myUser.getId());
            if (!trips.isEmpty()) {
                for (Trip trip : trips) {
                    String strTrip = String.format("Id: %s \n" +
                            "Транспорт: %s \n" +
                            "Начало поездки: %s \n" +
                            "Конец поездки: %s \n" +
                            "Стоимость поездки: %s\n", trip.getId(), trip.getVehicle().getType().toString(), trip.getStartTime(), trip.getEndTime(), trip.getConstTravel());
                    utils.send(absSender, chat.getId(), strTrip, ParseMode.HTML, false);
                }
            } else {
                utils.send(absSender, chat.getId(), "Похоже у вас еще не было аренд", ParseMode.HTML, false);
            }
        }else {
            utils.send(absSender,chat.getId(),"Вы не зарегистированы в системе! Пройдите регистрацию!",ParseMode.HTML,false);
        }
    }catch (Exception e){
        utils.send(absSender, chat.getId(), "Непредвиденная ошибка, попробуйте снова!", ParseMode.HTML, false);
        System.out.println("Ошибка в TripInfoCommand");
    }
    }
}
