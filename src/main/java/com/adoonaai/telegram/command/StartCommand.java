package com.adoonaai.telegram.command;

import com.adoonaai.models.enums.Role;
import com.adoonaai.sevice.user.UserService;
import com.adoonaai.telegram.AbstractBotCommand;
import com.adoonaai.telegram.SendMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;


@Component
public class StartCommand extends AbstractBotCommand {

    private final SendMsg utils;
    private final UserService userService;

    @Autowired
    public StartCommand(SendMsg utils, UserService userService) {
        super("/start", "start command",utils);
        this.utils = utils;
        this.userService = userService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

            try{
                utils.send(absSender, chat.getId(), "Добро пожаловать в WoodyRent", ParseMode.HTML, false);
            if (userService.userExistByLogin(user.getUserName())){
                utils.send(absSender, chat.getId(), String.format("Здравствуйте %s", user.getUserName()), ParseMode.HTML, false);
            }else {
                com.adoonaai.models.User myUser = new com.adoonaai.models.User();
                myUser.setLogin(user.getUserName());
                myUser.setRole(Role.USER);
                myUser.setPassword("112233");
                userService.createUser(myUser);

                String start = String.format("%s, Вы зарегистрировались! \n" +
                                            "Для навигации нажмите /help",myUser.getLogin());

                utils.send(absSender, chat.getId(), start, ParseMode.HTML, false);
            }
            }catch (Exception e){
                utils.send(absSender, chat.getId(), "Непредвиденная ошибка, попробуйте снова!", ParseMode.HTML, false);
                System.out.println("Ошибка в StartCommand");
            }
    }

}
