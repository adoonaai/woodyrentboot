package com.adoonaai.telegram.command;

import com.adoonaai.sevice.user.UserService;
import com.adoonaai.telegram.AbstractBotCommand;
import com.adoonaai.telegram.SendMsg;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Component
public class HelpCommand  extends AbstractBotCommand {

    private UserService userService;

    public HelpCommand(SendMsg utils, UserService userService) {
        super("/help", "help command", utils);
        this.userService = userService;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

        try {
            if (userService.userExistByLogin(user.getUserName())) {
            utils.send(absSender,chat.getId(),"Навигация", ParseMode.HTML,false);
                String help = "/info - информация о пользователе \n" +
                              "/infotrip - информация о совершенных поездках \n" +
                              "/search - поиск свободных ТС \n" +
                              "/trip модель ТС - начало аренды \n" +
                              "/finish модель ТС - конец аренды \n";
                utils.send(absSender, chat.getId(), help,ParseMode.HTML,false);
            }else {
                utils.send(absSender,chat.getId(),"Вы не зарегистрированы в системе! Пройдите регистрацию",ParseMode.HTML,false);
            }
        }catch (Exception e){
            utils.send(absSender, chat.getId(), "Непредвиденная ошибка, попробуйте снова!", ParseMode.HTML, false);
            System.out.println("Ошибка в HelpCommand");
        }

    }
}
