package com.adoonaai.exception;

public class BusinessRuntimeException extends RuntimeException{

    private ErrorCodeEnum errorCode;

    public BusinessRuntimeException(ErrorCodeEnum errorCode) {
        super(errorCode.getMessageTemplate());
    }

    public BusinessRuntimeException(String message, ErrorCodeEnum errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public BusinessRuntimeException(String message, Throwable cause, ErrorCodeEnum errorCode) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public BusinessRuntimeException(Throwable cause, ErrorCodeEnum errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public BusinessRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ErrorCodeEnum errorCode) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.errorCode = errorCode;
    }

    public BusinessRuntimeException(ErrorCodeEnum errorCode, Long id) {
        super(errorCode.getMessage(id));
    }

    public ErrorCodeEnum getErrorCode() {
        return errorCode;
    }
}
