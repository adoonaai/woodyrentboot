package com.adoonaai.exception;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.text.MessageFormat;

@RequiredArgsConstructor
public enum ErrorCodeEnum {

    USER_NOT_FOUND("Пользователь не найдет, id={0}",HttpStatus.NOT_FOUND),
    USER_NOT_MONEY("У пользователя недостаточно средств. Пополните баланс"),
    TRANSPORT_NOT_FOUND("Транспорт не найдет, id={0}",HttpStatus.NOT_FOUND),
    TRANSPORT_NOT_AVAILABLE("Транспорт занят",HttpStatus.NOT_FOUND),
    TRANSPORT_UNAVAILABLE("Невозможно закрыть не свой транспорт"),
    INTERNAL_SERVER_ERROR("Внутренняя ошибка сервера"),
    PARKING_NOT_FOUND("Парковка не найдена, id={0}",HttpStatus.NOT_FOUND);

    private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    private final String messageTemplate;


    ErrorCodeEnum(String messageTemplate,HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
        this.messageTemplate = messageTemplate;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessageTemplate() {
        return messageTemplate;
    }

    public String getMessage(Object... args) {
        return MessageFormat.format(messageTemplate,args);
    }
}
