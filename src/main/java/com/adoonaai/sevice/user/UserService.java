package com.adoonaai.sevice.user;

import com.adoonaai.dto.UserUi;
import com.adoonaai.models.User;

import java.util.List;


public interface UserService {

    void createUser(User user);
    List<User> getAllUsers();
    User findById(Long id);

    UserUi getCurrent();

    User findByLogin(String login);
    boolean userExistByLogin(String login);

    boolean updateUser(User user);
    boolean deleteUser(Long id);



}
