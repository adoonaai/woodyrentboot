package com.adoonaai.sevice.user;


import com.adoonaai.access.UserRepository;
import com.adoonaai.dto.UserUi;
import com.adoonaai.exception.BusinessRuntimeException;
import com.adoonaai.models.User;
import com.adoonaai.exception.ErrorCodeEnum;
import com.adoonaai.security.CallContext;
import com.adoonaai.security.SecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void createUser(User user) {
        //Проверка
        user.setBalance(BigDecimal.valueOf(500));
        userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        //Проверка
        return userRepository.findAll();
    }

    @Override
    public User findById(Long id) {

        return userRepository.findById(id).orElseThrow(() -> new BusinessRuntimeException(ErrorCodeEnum.USER_NOT_FOUND,id));
    }

    @Override
    public boolean updateUser(User user) {

            userRepository.save(user);
            return true;
    }

    @Override
    public boolean deleteUser(Long id) {

        User userCheck = findById(id);
        if (userCheck != null) {
            userRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public boolean userExistByLogin(String login) {
        User user = userRepository.findByLogin(login);
        return user !=null;
    }

    @Override
    public UserUi getCurrent() {

        CallContext context = SecurityContext.get();
        User user = userRepository.findById(context.getUserId()).orElseThrow();
        return UserUi.builder()
                .id(user.getId())
                .login(user.getLogin())
                .role(String.valueOf(user.getRole()))
                .balance(user.getBalance())
                .build();


    }

    @Override
    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }



}
