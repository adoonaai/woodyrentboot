package com.adoonaai.sevice.parking;


import com.adoonaai.models.Parking;

import java.util.List;

public interface ParkingService {

    List<Parking> getAllParkingZone();

    Parking findById(Long id);

    Parking createParking(Parking parking);

    boolean updateParking(Parking parking);

    boolean deleteParking(Long id);
}
