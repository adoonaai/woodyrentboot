package com.adoonaai.sevice.parking;

import com.adoonaai.access.ParkingRepository;
import com.adoonaai.exception.BusinessRuntimeException;
import com.adoonaai.exception.ErrorCodeEnum;
import com.adoonaai.models.Parking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParkingServiceImpl implements ParkingService{

    private final ParkingRepository parkingRepository;

    @Autowired
    public ParkingServiceImpl(ParkingRepository parkingRepository) {
        this.parkingRepository = parkingRepository;
    }

    @Override
    public Parking createParking(Parking parking) {
        //Проверка
        return parkingRepository.save(parking);
    }

    @Override
    public List<Parking> getAllParkingZone(){
        //Проверка
        return parkingRepository.findAll();
    }

    @Override
    public Parking findById(Long id) {

        return parkingRepository.findById(id).orElseThrow(() -> new BusinessRuntimeException(ErrorCodeEnum.PARKING_NOT_FOUND,id));
    }


    @Override
    public boolean updateParking(Parking parking) {

            parkingRepository.save(parking);
            return true;
    }

    @Override
    public boolean deleteParking(Long id) {

        Parking parking = findById(id);
        if (parking != null) {
            parkingRepository.deleteById(id);
            return true;
        }
        return false;
    }

}
