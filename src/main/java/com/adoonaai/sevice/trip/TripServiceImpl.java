package com.adoonaai.sevice.trip;


import com.adoonaai.access.TripRepository;
import com.adoonaai.dto.CreateTripRequestDto;
import com.adoonaai.exception.BusinessRuntimeException;
import com.adoonaai.models.*;
import com.adoonaai.exception.ErrorCodeEnum;
import com.adoonaai.models.enums.Role;
import com.adoonaai.models.enums.TripStatus;
import com.adoonaai.models.enums.VehicleStatus;
import com.adoonaai.sevice.user.UserService;
import com.adoonaai.sevice.vehicle.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class TripServiceImpl implements TripService {

    private final TripRepository tripRepository;
    private final VehicleService vehicleService;
    private final UserService userService;

    @Autowired
    public TripServiceImpl(TripRepository tripRepository, VehicleService vehicleService, UserService userService) {
        this.tripRepository = tripRepository;
        this.vehicleService = vehicleService;
        this.userService = userService;
    }


    @Transactional
    @Override
    public synchronized void  createTrip(CreateTripRequestDto requestDto) {

        User user = userService.findById(requestDto.getUserId());
        Vehicle vehicle = vehicleService.findById(requestDto.getVehicleId());

        if (!(user.getBalance().compareTo(BigDecimal.valueOf(100)) >= 0)){
            throw new BusinessRuntimeException(ErrorCodeEnum.USER_NOT_MONEY);
        }

        if (vehicle.getStatus().equals(VehicleStatus.BUSY)){
            throw new BusinessRuntimeException(ErrorCodeEnum.TRANSPORT_NOT_AVAILABLE,vehicle.getId());
        }

        Trip trip = Trip.builder()
                .startTime(LocalDateTime.now())
                .startParking(vehicle.getParking())
                .status(TripStatus.CREATED)
                .user(user)
                .vehicle(vehicle)
                .build();
        tripRepository.save(trip);

        vehicle.setStatus(VehicleStatus.BUSY);
        vehicleService.updateVehicle(vehicle);
    }

    @Override
    public void closingTrip(Long tripId) {

        Trip trip = tripRepository.findById(tripId).orElseThrow();
        User user = userService.findById(trip.getUser().getId());
        Vehicle vehicle = vehicleService.findById(trip.getVehicle().getId());

        //возможно тут еще нужна проверка на id в trip что он существует
        if (vehicle.getStatus().equals(VehicleStatus.FREE)){
            throw new BusinessRuntimeException(ErrorCodeEnum.INTERNAL_SERVER_ERROR);
        }
        //тут можно поставить изменение парковки
//        vehicle.setParking();

        LocalDateTime start = trip.getStartTime();
        LocalDateTime end = LocalDateTime.now();
        Duration duration = Duration.between(start,end);
        long minutesOnWay = duration.toMinutes()% 60;

        BigDecimal x1 = BigDecimal.valueOf(minutesOnWay);
        BigDecimal x2 = x1.multiply(vehicle.getPricePerMinute());
        BigDecimal costTravel = x2.add(vehicle.getStartPrice());

        user.setBalance(user.getBalance().subtract(costTravel));
        userService.createUser(user);

        trip.setEndTime(LocalDateTime.now());
        trip.setEndParking(vehicle.getParking());
        trip.setStatus(TripStatus.COMPLETED);
        trip.setConstTravel(costTravel);
        tripRepository.save(trip);

        vehicle.setStatus(VehicleStatus.FREE);
        vehicleService.updateVehicle(vehicle);

    }


    @Override
    public List<Trip> findAllTrips(Long id, Role role) {

        if (role.equals(Role.ADMIN)){
            return tripRepository.findAll();
        }else {
            return tripRepository.findByUserId(id);
        }
    }

    @Override
    public List<Trip> findTripByUserId(Long id) {
        return tripRepository.findByUserId(id);
    }


    @Override
    public Trip findTripBusyByVehicleId(Vehicle vehicle) {
        return tripRepository.findByBusyVehicleId(vehicle);
    }
}
