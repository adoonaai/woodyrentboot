package com.adoonaai.sevice.trip;

import com.adoonaai.dto.CreateTripRequestDto;

import com.adoonaai.models.Trip;
import com.adoonaai.models.Vehicle;
import com.adoonaai.models.enums.Role;

import java.util.List;

public interface TripService {

    void createTrip(CreateTripRequestDto requestDto);
    void closingTrip(Long tripId);

    List<Trip> findTripByUserId(Long id);

    Trip findTripBusyByVehicleId(Vehicle vehicle);

    List<Trip> findAllTrips(Long id, Role role);
}
