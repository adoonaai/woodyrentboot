package com.adoonaai.sevice.vehicle;


import com.adoonaai.access.VehicleRepository;
import com.adoonaai.converters.VehicleConverter;
import com.adoonaai.dto.VehicleDto;
import com.adoonaai.exception.BusinessRuntimeException;
import com.adoonaai.exception.ErrorCodeEnum;
import com.adoonaai.models.Bike;
import com.adoonaai.models.Parking;
import com.adoonaai.models.Scooter;
import com.adoonaai.models.Vehicle;
import com.adoonaai.models.enums.ParkingType;
import com.adoonaai.models.enums.VehicleType;
import com.adoonaai.models.enums.VehicleStatus;
import com.adoonaai.sevice.parking.ParkingService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;
    private final VehicleConverter vehicleConverter;
    private final ParkingService parkingService;


    public VehicleServiceImpl(VehicleRepository vehicleRepository, VehicleConverter vehicleConverter, ParkingService parkingService) {
        this.vehicleRepository = vehicleRepository;
        this.vehicleConverter = vehicleConverter;
        this.parkingService = parkingService;
    }

    @Override
    public void createVehicle(VehicleDto vehicleDto) {

        Parking parking = parkingService.findById(vehicleDto.getParking_id());
        String parkingType= parking.getType().toString();


        if (vehicleDto.getType().equals(VehicleType.SCOOTER) && (parkingType.equals("ALL") | parkingType.equals(VehicleType.SCOOTER.toString()))) {
            Scooter scooter = vehicleConverter.fromVehicleDtoToScooter(vehicleDto);
            scooter.setLongitude(parking.getLongitude());
            scooter.setLatitude(parking.getLatitude());
            scooter.setParking(parking);
            vehicleRepository.save(scooter);
        } else if (vehicleDto.getType().equals(VehicleType.BIKE)){
            Bike bike = vehicleConverter.fromVehicleDtoToBike(vehicleDto);
            bike.setLongitude(parking.getLongitude());
            bike.setLatitude(parking.getLatitude());
            bike.setParking(parking);
            vehicleRepository.save(bike);
        }else {
            throw new BusinessRuntimeException(ErrorCodeEnum.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public void updateVehicle(Vehicle vehicle) {
        vehicleRepository.save(vehicle);
    }

    @Override
    public List<Vehicle> getAllVehicle() {

        return vehicleRepository.findAll();
    }

    @Override
    public List<Vehicle> getAllVehicleAvailable(){

        return vehicleRepository.findByVehicleStatus(VehicleStatus.FREE);
    }


    @Override
    public Vehicle findByModel(String model) {
        return vehicleRepository.findByModel(model);
    }

    @Override
    public Vehicle findById(Long id) {

        return vehicleRepository.findById(id).orElseThrow();
    }

    @Override
    public List<Vehicle> getAllScooters() {


        return vehicleRepository.findByTypeVehicle(VehicleType.SCOOTER);
    }

    @Override
    public List<Vehicle> getAllBike() {

        return vehicleRepository.findByTypeVehicle(VehicleType.BIKE);
    }


    @Override
    public boolean deleteVehicle(Long id) {

        vehicleRepository.deleteById(id);

        return true;
    }



}




