package com.adoonaai.sevice.vehicle;

import com.adoonaai.dto.VehicleDto;
import com.adoonaai.models.Bike;
import com.adoonaai.models.Scooter;
import com.adoonaai.models.Vehicle;
import com.adoonaai.models.enums.VehicleStatus;

import java.util.List;

public interface VehicleService {

    void createVehicle(VehicleDto vehicleDto);

    void updateVehicle(Vehicle vehicle);

    List<Vehicle> getAllVehicle();

    List<Vehicle> getAllVehicleAvailable();

    Vehicle findByModel(String model);

    List<Vehicle> getAllScooters();

    List<Vehicle> getAllBike();

    Vehicle findById(Long id);

    boolean deleteVehicle(Long id);

}
