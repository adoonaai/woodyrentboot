package com.adoonaai.converters;

import com.adoonaai.dto.VehicleDto;
import com.adoonaai.models.Bike;
import com.adoonaai.models.Scooter;
import org.springframework.stereotype.Service;

@Service
public class VehicleConverter {

    public Scooter fromVehicleDtoToScooter(VehicleDto vehicleDto){
        Scooter scooter = new Scooter();
        scooter.setId(vehicleDto.getId());
        scooter.setModel(vehicleDto.getModel());
        scooter.setType(vehicleDto.getType());
        scooter.setCondition(vehicleDto.getCondition());
        scooter.setStartPrice(vehicleDto.getStartPrice());
        scooter.setPricePerMinute(vehicleDto.getPricePerMinute());
        scooter.setEngineCharge(vehicleDto.getEngineCharge());
        scooter.setMaxSpeed(vehicleDto.getMaxSpeed());
        scooter.setStatus(vehicleDto.getStatus());
        return scooter;
    }

    public Bike fromVehicleDtoToBike(VehicleDto vehicleDto){
        Bike bike = new Bike();
        bike.setId(vehicleDto.getId());
        bike.setModel(vehicleDto.getModel());
        bike.setType(vehicleDto.getType());
        bike.setCondition(vehicleDto.getCondition());
        bike.setStartPrice(vehicleDto.getStartPrice());
        bike.setPricePerMinute(vehicleDto.getPricePerMinute());
        bike.setStatus(vehicleDto.getStatus());
        return bike;
    }

}
