package com.adoonaai.dto;

import com.adoonaai.models.Trip;
import com.adoonaai.models.User;
import com.adoonaai.models.Vehicle;
import com.adoonaai.models.enums.TripStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateTripRequestDto {

    private Long userId;
    private Long vehicleId;


}
