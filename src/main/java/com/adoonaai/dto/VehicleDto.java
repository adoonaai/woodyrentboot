package com.adoonaai.dto;


import com.adoonaai.models.enums.Condition;
import com.adoonaai.models.enums.VehicleType;
import com.adoonaai.models.enums.VehicleStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VehicleDto {

    private Long id;
    private String model;
    private VehicleType type;
    private Condition condition;
    private VehicleStatus status;
//    private double longitude;
//    private double latitude;
    private BigDecimal startPrice;
    private BigDecimal pricePerMinute;
    private int maxSpeed;
    private int engineCharge;
    private Long parking_id;


}
