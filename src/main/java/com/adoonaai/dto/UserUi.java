package com.adoonaai.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class UserUi {

    private Long id;
    private String login;
    private String role;
    private BigDecimal balance;

}
